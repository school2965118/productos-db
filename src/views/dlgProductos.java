/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package views;

/**
 *
 * @author Anton
 */
public class dlgProductos extends javax.swing.JDialog {

    /**
     * Creates new form dlgProductos
     */
    public dlgProductos(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tabbedPane = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        btnInsertar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnBuscar = new javax.swing.JButton();
        btnDeshabilitar = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        txtCodigoProducto = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtNombeProducto = new javax.swing.JTextField();
        txtPrecioProducto = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        spinStatusProducto = new javax.swing.JSpinner();
        jSeparator1 = new javax.swing.JSeparator();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        calFecjaProducto = new com.toedter.calendar.JCalendar();
        jLabel7 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        btnNuevo = new javax.swing.JButton();
        btnCerrarse = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        txtCodigoProducto1 = new javax.swing.JTextField();
        btnBuscar1 = new javax.swing.JButton();
        btnHabilitar = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JSeparator();
        jLabel9 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        txtNombeProducto1 = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tabbedPane.setName(""); // NOI18N

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnInsertar.setBackground(java.awt.Color.white);
        btnInsertar.setFont(new java.awt.Font("Montserrat", 1, 12)); // NOI18N
        btnInsertar.setForeground(java.awt.Color.darkGray);
        btnInsertar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Assets/Synchronize - 192x192.png"))); // NOI18N
        btnInsertar.setText("Guardar");
        btnInsertar.setToolTipText("");
        btnInsertar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInsertarActionPerformed(evt);
            }
        });
        jPanel1.add(btnInsertar, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 40, 120, 40));

        btnCancelar.setBackground(java.awt.SystemColor.control);
        btnCancelar.setFont(new java.awt.Font("Montserrat", 1, 12)); // NOI18N
        btnCancelar.setForeground(java.awt.Color.darkGray);
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Assets/Close Circle 1 - 192x192.png"))); // NOI18N
        btnCancelar.setEnabled(false);
        btnCancelar.setLabel("Cancelar");
        jPanel1.add(btnCancelar, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 140, 120, 40));

        btnBuscar.setBackground(java.awt.SystemColor.controlLtHighlight);
        btnBuscar.setFont(new java.awt.Font("Montserrat", 1, 8)); // NOI18N
        btnBuscar.setForeground(java.awt.Color.darkGray);
        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Assets/Search - 192x192.png"))); // NOI18N
        btnBuscar.setText("Buscar");
        jPanel1.add(btnBuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 40, 90, 30));

        btnDeshabilitar.setBackground(java.awt.Color.white);
        btnDeshabilitar.setFont(new java.awt.Font("Montserrat", 1, 8)); // NOI18N
        btnDeshabilitar.setForeground(java.awt.Color.darkGray);
        btnDeshabilitar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Assets/Close Circle 1 - 192x192.png"))); // NOI18N
        btnDeshabilitar.setText("Deshabilitar");
        btnDeshabilitar.setEnabled(false);
        btnDeshabilitar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeshabilitarActionPerformed(evt);
            }
        });
        jPanel1.add(btnDeshabilitar, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 90, 120, 40));

        jLabel2.setFont(new java.awt.Font("Montserrat", 0, 12)); // NOI18N
        jLabel2.setText("Resultados:");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 210, -1, -1));

        txtCodigoProducto.setToolTipText("");
        txtCodigoProducto.setEnabled(false);
        txtCodigoProducto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCodigoProductoKeyTyped(evt);
            }
        });
        jPanel1.add(txtCodigoProducto, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 270, 30));

        jLabel3.setFont(new java.awt.Font("Montserrat", 0, 12)); // NOI18N
        jLabel3.setText("Nombre");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 80, 60, -1));

        jLabel4.setFont(new java.awt.Font("Montserrat", 0, 12)); // NOI18N
        jLabel4.setText("Precio");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 140, 60, -1));

        txtNombeProducto.setToolTipText("");
        txtNombeProducto.setEnabled(false);
        txtNombeProducto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNombeProductoKeyTyped(evt);
            }
        });
        jPanel1.add(txtNombeProducto, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 100, 360, 30));

        txtPrecioProducto.setToolTipText("");
        txtPrecioProducto.setEnabled(false);
        txtPrecioProducto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPrecioProductoKeyTyped(evt);
            }
        });
        jPanel1.add(txtPrecioProducto, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 160, 230, 30));

        jLabel5.setFont(new java.awt.Font("Montserrat", 0, 12)); // NOI18N
        jLabel5.setText("Status");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 140, 60, -1));

        spinStatusProducto.setModel(new javax.swing.SpinnerNumberModel(0, 0, 1, 1));
        spinStatusProducto.setEnabled(false);
        jPanel1.add(spinStatusProducto, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 160, 120, 30));

        jSeparator1.setForeground(javax.swing.UIManager.getDefaults().getColor("Button.disabledToolBarBorderBackground"));
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 200, 850, -1));

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTable1.setGridColor(new java.awt.Color(204, 204, 204));
        jTable1.setIntercellSpacing(new java.awt.Dimension(2, 2));
        jScrollPane1.setViewportView(jTable1);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 230, 850, 110));

        jLabel6.setFont(new java.awt.Font("Montserrat", 0, 12)); // NOI18N
        jLabel6.setText("Código ");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        calFecjaProducto.setDecorationBackgroundColor(java.awt.SystemColor.control);
        calFecjaProducto.setEnabled(false);
        calFecjaProducto.setSundayForeground(javax.swing.UIManager.getDefaults().getColor("OptionPane.errorDialog.titlePane.shadow"));
        calFecjaProducto.setWeekdayForeground(javax.swing.UIManager.getDefaults().getColor("Button.darkShadow"));
        jPanel1.add(calFecjaProducto, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 40, -1, -1));

        jLabel7.setFont(new java.awt.Font("Montserrat", 0, 12)); // NOI18N
        jLabel7.setText("Fecha");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 20, 60, -1));

        jLabel1.setFont(new java.awt.Font("Montserrat", 0, 12)); // NOI18N
        jLabel1.setText("Acciones");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 20, 60, -1));

        btnNuevo.setBackground(java.awt.Color.white);
        btnNuevo.setFont(new java.awt.Font("Montserrat", 1, 12)); // NOI18N
        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Assets/Plus.png"))); // NOI18N
        btnNuevo.setText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        jPanel1.add(btnNuevo, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 40, 120, 40));

        btnCerrarse.setBackground(java.awt.Color.red);
        btnCerrarse.setFont(new java.awt.Font("Montserrat", 1, 12)); // NOI18N
        btnCerrarse.setForeground(java.awt.Color.white);
        btnCerrarse.setText("Cerrar");
        jPanel1.add(btnCerrarse, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 140, 120, 40));

        btnLimpiar.setBackground(java.awt.SystemColor.controlLtHighlight);
        btnLimpiar.setFont(new java.awt.Font("Montserrat", 1, 12)); // NOI18N
        btnLimpiar.setForeground(java.awt.Color.darkGray);
        btnLimpiar.setText("Limpiar");
        jPanel1.add(btnLimpiar, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 90, 120, 40));

        tabbedPane.addTab("Añadir / Actualizar Productos", jPanel1);

        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel8.setFont(new java.awt.Font("Montserrat", 0, 12)); // NOI18N
        jLabel8.setText("Código ");
        jPanel2.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        txtCodigoProducto1.setToolTipText("");
        txtCodigoProducto1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCodigoProducto1KeyReleased(evt);
            }
        });
        jPanel2.add(txtCodigoProducto1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 270, 30));

        btnBuscar1.setBackground(java.awt.SystemColor.control);
        btnBuscar1.setFont(new java.awt.Font("Montserrat", 1, 12)); // NOI18N
        btnBuscar1.setForeground(java.awt.Color.darkGray);
        btnBuscar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Assets/Search - 192x192.png"))); // NOI18N
        btnBuscar1.setText("Buscar");
        btnBuscar1.setEnabled(false);
        jPanel2.add(btnBuscar1, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 30, 120, 40));

        btnHabilitar.setBackground(java.awt.SystemColor.control);
        btnHabilitar.setFont(new java.awt.Font("Montserrat", 1, 12)); // NOI18N
        btnHabilitar.setForeground(java.awt.Color.darkGray);
        btnHabilitar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Assets/Plus.png"))); // NOI18N
        btnHabilitar.setText("Habilitar");
        btnHabilitar.setEnabled(false);
        jPanel2.add(btnHabilitar, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 30, 120, 40));

        jSeparator3.setForeground(javax.swing.UIManager.getDefaults().getColor("Button.disabledToolBarBorderBackground"));
        jPanel2.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 80, 840, 10));

        jLabel9.setFont(new java.awt.Font("Montserrat", 0, 12)); // NOI18N
        jLabel9.setText("Resultados:");
        jPanel2.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, -1, -1));

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTable2.setGridColor(new java.awt.Color(204, 204, 204));
        jTable2.setIntercellSpacing(new java.awt.Dimension(2, 2));
        jScrollPane2.setViewportView(jTable2);

        jPanel2.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, 840, 210));

        txtNombeProducto1.setToolTipText("");
        txtNombeProducto1.setDisabledTextColor(new java.awt.Color(51, 51, 51));
        txtNombeProducto1.setEnabled(false);
        jPanel2.add(txtNombeProducto1, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 40, 270, 30));

        jLabel10.setFont(new java.awt.Font("Montserrat", 0, 12)); // NOI18N
        jLabel10.setText("Nombre");
        jPanel2.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 20, 60, -1));

        tabbedPane.addTab("Productos Deshabilitados", jPanel2);

        getContentPane().add(tabbedPane, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 890, 380));
        tabbedPane.getAccessibleContext().setAccessibleName("");
        tabbedPane.getAccessibleContext().setAccessibleDescription("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnInsertarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInsertarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnInsertarActionPerformed

    private void btnDeshabilitarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeshabilitarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnDeshabilitarActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void txtCodigoProductoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCodigoProductoKeyTyped
        int key = evt.getKeyChar();
        boolean numbers = key >= 48 && key <= 57;
        if (!numbers)
            evt.consume();

    }//GEN-LAST:event_txtCodigoProductoKeyTyped

    private void txtNombeProductoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombeProductoKeyTyped
        int key = evt.getKeyChar();
        boolean mayus = key >= 65 && key <= 90;
        boolean minus = key >= 97 && key <= 122;
        boolean space = key == 32;
        boolean numbers = key >= 48 && key <= 57;
        if (!(mayus || minus || space || numbers))
            evt.consume();
    }//GEN-LAST:event_txtNombeProductoKeyTyped

    private void txtPrecioProductoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPrecioProductoKeyTyped
        int key = evt.getKeyChar();
        boolean numbers = key >= 48 && key <= 57;
        boolean point = key == 46;
        if (!numbers && !point)
            evt.consume();

    }//GEN-LAST:event_txtPrecioProductoKeyTyped

    private void txtCodigoProducto1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCodigoProducto1KeyReleased
        habilitarBotonGuardar();
    }//GEN-LAST:event_txtCodigoProducto1KeyReleased

    /**
     * @param args the command line arguments
     */
    private void habilitarBotonGuardar(){
    if(!txtCodigoProducto1.getText().isEmpty()){
        btnBuscar1.setEnabled(true);
    }
    else{
        btnBuscar1.setEnabled(false);
    }
}

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(dlgProductos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(dlgProductos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(dlgProductos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(dlgProductos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                dlgProductos dialog = new dlgProductos(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnBuscar;
    public javax.swing.JButton btnBuscar1;
    public javax.swing.JButton btnCancelar;
    public javax.swing.JButton btnCerrarse;
    public javax.swing.JButton btnDeshabilitar;
    public javax.swing.JButton btnHabilitar;
    public javax.swing.JButton btnInsertar;
    public javax.swing.JButton btnLimpiar;
    public javax.swing.JButton btnNuevo;
    public com.toedter.calendar.JCalendar calFecjaProducto;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator3;
    public javax.swing.JTable jTable1;
    public javax.swing.JTable jTable2;
    public javax.swing.JSpinner spinStatusProducto;
    public javax.swing.JTabbedPane tabbedPane;
    public javax.swing.JTextField txtCodigoProducto;
    public javax.swing.JTextField txtCodigoProducto1;
    public javax.swing.JTextField txtNombeProducto;
    public javax.swing.JTextField txtNombeProducto1;
    public javax.swing.JTextField txtPrecioProducto;
    // End of variables declaration//GEN-END:variables
}
