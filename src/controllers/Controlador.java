package controllers;
import views.dlgProductos;
import models.DBProducto;
import models.Productos;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.ParseException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;



public class Controlador implements ActionListener {
    
    private Productos pro;
    private dlgProductos vista;
    private DBProducto dbpro;
    private DefaultTableModel tabla;
    private boolean isInsertar = false;
    
    public Controlador(Productos pro, dlgProductos vista, DBProducto dbpro) {
    this.pro = pro;
    this.vista = vista;
    this.dbpro = dbpro;
    tabla = new DefaultTableModel();
    
    // Hacer que el controlador ESCUCHE los botones de la vista
    vista.btnCancelar.addActionListener(this);
    vista.btnBuscar.addActionListener(this);
    vista.btnDeshabilitar.addActionListener(this);
    vista.btnHabilitar.addActionListener(this);
    vista.btnInsertar.addActionListener(this);
    vista.btnBuscar1.addActionListener(this);
    vista.btnCancelar.addActionListener(this);
    vista.btnCerrarse.addActionListener(this);
    vista.btnNuevo.addActionListener(this);
    vista.btnCancelar.addActionListener(this);
    vista.btnLimpiar.addActionListener(this);
    
    // Agregando el listener al JTabbedPane
    vista.tabbedPane.addChangeListener(new ChangeListener() {
        @Override
        public void stateChanged(ChangeEvent e) {
            JTabbedPane sourceTabbedPane = (JTabbedPane) e.getSource();
            int index = sourceTabbedPane.getSelectedIndex();

            switch(index) {
                case 0: 
                    // El usuario ha seleccionado la primera pestaña
                    rellenarTabla(); 
                    break;
                case 1:
                    // El usuario ha seleccionado la segunda pestaña
                    rellenarTablaDesactivados(); 
                    break;
            }
        }
    });
    rellenarTabla();
    rellenarTablaDesactivados();
}
    
    private void iniciarVista() {
        vista.setTitle("Productos Base de Datos");
        vista.setSize(920, 430);
        vista.setLocationRelativeTo(null);
        vista.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        vista.setVisible(true);
        String[] columnNames = {"idProducto", "codigo", "nombre", "precio", "fecha", "status"};
        DefaultTableModel model = new DefaultTableModel(columnNames, 0);
        vista.jTable1.setModel(model);
        vista.jTable2.setModel(model);
        vista.dispose();
        System.exit(0); // esto finaliza la aplicación
    }

    public void rellenarTabla() {
        try {
            // Definir los nombres de las columnas
            String[] columnNames = {"idProducto", "codigo", "nombre", "precio", "fecha", "status"};

            // Obtener los datos de la base de datos
            List<Productos> productos = dbpro.listar();

            // Crear una matriz para los datos de la tabla
            Object[][] data = new Object[productos.size()][columnNames.length];

            // Llenar la matriz con los datos de los productos
            for (int i = 0; i < productos.size(); i++) {
                Productos producto = productos.get(i);
                data[i][0] = producto.getIdProducto();
                data[i][1] = producto.getCodigo();
                data[i][2] = producto.getNombre();
                data[i][3] = producto.getPrecio();
                data[i][4] = producto.getFecha();
                data[i][5] = producto.getStatus();
            }

            // Crear un modelo de tabla con los datos y los nombres de las columnas
            DefaultTableModel model = new DefaultTableModel(data, columnNames);

            // Aplicar el modelo a la tabla
            vista.jTable1.setModel(model);
        } catch (Exception ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void rellenarTablaDesactivados(){
                try {
            // Definir los nombres de las columnas
            String[] columnNames = {"idProducto", "codigo", "nombre", "precio", "fecha", "status"};

            // Obtener los datos de la base de datos
            List<Productos> productos = dbpro.listarDesactivados();

            // Crear una matriz para los datos de la tabla
            Object[][] data = new Object[productos.size()][columnNames.length];

            // Llenar la matriz con los datos de los productos
            for (int i = 0; i < productos.size(); i++) {
                Productos producto = productos.get(i);
                data[i][0] = producto.getIdProducto();
                data[i][1] = producto.getCodigo();
                data[i][2] = producto.getNombre();
                data[i][3] = producto.getPrecio();
                data[i][4] = producto.getFecha();
                data[i][5] = producto.getStatus();
            }

            // Crear un modelo de tabla con los datos y los nombres de las columnas
            DefaultTableModel model = new DefaultTableModel(data, columnNames);

            // Aplicar el modelo a la tabla
            vista.jTable2.setModel(model);
        } catch (Exception ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void limpiar(){
        vista.txtCodigoProducto.setText("");
        vista.txtNombeProducto.setText("");
        vista.txtPrecioProducto.setText("");
        vista.spinStatusProducto.setValue(0);
    }
    
    public void limpiar2(){
        vista.txtCodigoProducto1.setText("");
        vista.txtNombeProducto1.setText("");
    }
    
    public void deshabilitar(){
        vista.txtCodigoProducto.setEnabled(false);
        vista.txtNombeProducto.setEnabled(false);
        vista.txtPrecioProducto.setEnabled(false);
        vista.spinStatusProducto.setEnabled(false);
        vista.calFecjaProducto.setEnabled(false);
        vista.btnBuscar.setEnabled(false);
        vista.btnCancelar.setEnabled(false);
    }
    
    public void habilitar(){
        vista.txtCodigoProducto.setEnabled(true);
        vista.txtNombeProducto.setEnabled(true);
        vista.txtPrecioProducto.setEnabled(true);
        vista.spinStatusProducto.setEnabled(true);
        vista.calFecjaProducto.setEnabled(true);
        vista.btnBuscar.setEnabled(true);
        vista.btnCancelar.setEnabled(true);
    }
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
       
       if(e.getSource()== vista.btnCerrarse){
           int result = JOptionPane.showConfirmDialog(vista, "¿Seguro que deseas salir?", "Advertencia", JOptionPane.YES_NO_OPTION);
           if (result == JOptionPane.YES_OPTION)
               System.exit(0);
           else if (result == JOptionPane.NO_OPTION)
               vista.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
       }
       
        if (e.getSource() == vista.btnInsertar) {
            try {
                
                pro.setCodigo(vista.txtCodigoProducto.getText());
                pro.setNombre(vista.txtNombeProducto.getText());
                pro.setPrecio(Float.parseFloat(vista.txtPrecioProducto.getText()));
                pro.setStatus((int) vista.spinStatusProducto.getValue());
                Date fechaSeleccionada = vista.calFecjaProducto.getDate();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // Formato de fecha deseado
                String fechaString = sdf.format(fechaSeleccionada);
                pro.setFecha(fechaString);
                
                if(!dbpro.isExiste(vista.txtCodigoProducto.getText())){
                    isInsertar = true;
                    if (isInsertar) {
                        dbpro.insertar(pro);
                        JOptionPane.showMessageDialog(null, "Registro Guardado");
                        limpiar();
                        deshabilitar();
                        rellenarTabla();
                    }
                }
                else {
                    dbpro.actualizar(pro);
                    JOptionPane.showMessageDialog(null, "Registro actualizado");
                    rellenarTabla();
                    limpiar();
                    deshabilitar();
                }
            } catch (SQLException d) {
                JOptionPane.showMessageDialog(null, "Error al Guardar" + d);
            } catch (Exception ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        if (e.getSource() == vista.btnBuscar) {
            try {
                Productos res = new Productos();
                String codigo = vista.txtCodigoProducto.getText();
                if (dbpro.isExiste(vista.txtCodigoProducto.getText())) {
                    res = (Productos) dbpro.buscar(codigo);
                    JOptionPane.showMessageDialog(vista, "Producto " + res.getNombre() + " encontrado");
                    vista.txtCodigoProducto.setText(res.getCodigo());
                    vista.txtNombeProducto.setText(res.getNombre());
                    vista.txtPrecioProducto.setText(Float.toString(res.getPrecio()));
                    vista.spinStatusProducto.setValue(res.getStatus());
                    vista.btnDeshabilitar.setEnabled(true);
                    vista.btnHabilitar.setEnabled(true);
                    String fechaString = res.getFecha();
                    if (fechaString != null) {
                        try {
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // Formato de fecha deseado
                            Date fecha = sdf.parse(fechaString); // Convertir el String a Date
                            vista.calFecjaProducto.setDate(fecha); // Establecer la fecha en el componente JCalendar
                        } catch (ParseException par) {
                            // Manejar cualquier excepción si ocurre un error de formato de fecha
                            par.printStackTrace();
                        }
                    }
                }
                else
                {
                    JOptionPane.showMessageDialog(null, "No se pudo encontrar el producto con el código: " + codigo);
                }
            } catch (Exception ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        if (e.getSource() == vista.btnBuscar1) {
            try {
                Productos res = new Productos();
                int codigo = Integer.parseInt(vista.txtCodigoProducto1.getText());
                if (dbpro.isExiste(vista.txtCodigoProducto1.getText())) {
                    res = (Productos) dbpro.buscar(codigo);
                    JOptionPane.showMessageDialog(vista, "Producto " + res.getNombre() + " encontrado");
                    vista.txtCodigoProducto1.setText(res.getCodigo());
                    vista.txtNombeProducto1.setText(res.getNombre());
                    vista.btnHabilitar.setEnabled(true);
                }
                else
                {
                    JOptionPane.showMessageDialog(null, "No se pudo encontrar el producto con el código: " + codigo);
                }

            } catch (Exception ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        if (e.getSource() == vista.btnDeshabilitar) {
            String codigo = vista.txtCodigoProducto.getText();
            try {
                Productos pro = dbpro.buscar(codigo);
                if (pro != null) {
                    dbpro.deshabilitar(pro);
                    JOptionPane.showMessageDialog(null, "Producto Deshabilitado con Exito");
                    vista.spinStatusProducto.setValue(0); // El producto fue deshabilitado, así que establecer el estado a 0
                    rellenarTabla();
                    limpiar();
                    vista.btnDeshabilitar.setEnabled(false);
                } else {
                    JOptionPane.showMessageDialog(null, "No se pudo encontrar el producto con el código: " + codigo);
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Error al deshabilitar" + ex);
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        if (e.getSource() == vista.btnHabilitar){
            String codigo = vista.txtCodigoProducto1.getText();
            try {
                    Productos pro = dbpro.buscar(codigo);
                    if (pro != null){
                        dbpro.habilitar(pro);
                        JOptionPane.showMessageDialog(null, "Producto Habilitado con Exito");
                        limpiar2();
                        rellenarTablaDesactivados();
                    } else {
                        JOptionPane.showMessageDialog(null, "No se pudo encontrar el producto con el código: " + codigo);
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Error al Habilitar" + ex);
                    Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
        
        if (e.getSource() == vista.btnNuevo){
            habilitar();
        }
        
        if (e.getSource() == vista.btnLimpiar){
            limpiar();
        }
        
        if (e.getSource() == vista.btnCancelar){
            limpiar();
            deshabilitar();
        }
}

    public static void main(String[] args) {
       Productos pro = new Productos();
       DBProducto dbpro = new DBProducto();
       dlgProductos vista = new dlgProductos(new JFrame(),true);
       Controlador contra = new Controlador(pro,vista,dbpro);
       contra.iniciarVista();
    }

}
