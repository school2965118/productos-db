package models;
import java.util.ArrayList;
import java.sql.*;

public class DBProducto extends DbManejador implements DBPersistencia{

    @Override
    public void insertar(Object objeto) throws Exception {
        
        Productos pro = new Productos();
        pro = (Productos) objeto;
        String consulta = "Insert into productos(codigo,nombre,fecha,precio,status) values (?,?,?,?,?)";
        
        if (this.conectar()) {
            try {
                System.err.println("Se conecto correctamente");
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                // asignacion de valores a la consulta
                this.sqlConsulta.setString(1, pro.getCodigo());
                this.sqlConsulta.setString(2, pro.getNombre());
                this.sqlConsulta.setString(3, pro.getFecha());
                this.sqlConsulta.setFloat(4, pro.getPrecio());
                this.sqlConsulta.setInt(5, pro.getStatus());
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            } catch (SQLException e) {
                System.err.println("Surgio un error al insertar" + e.getMessage());
            }
        }
    }
    
@Override
public void actualizar(Object objeto) throws Exception {
    Productos pro = new Productos();
    pro = (Productos) objeto;

    String consulta = "UPDATE productos SET nombre = ?, fecha = ?, precio = ?, status = ? WHERE codigo = ?";

    if(this.conectar()) {
        try {
            System.err.println("Se conecto correctamente");
            this.sqlConsulta = this.conexion.prepareStatement(consulta);

            // asignacion de valores a la consulta
            this.sqlConsulta.setString(1, pro.getNombre());
            this.sqlConsulta.setString(2, pro.getFecha());
            this.sqlConsulta.setFloat(3, pro.getPrecio());
            this.sqlConsulta.setInt(4, pro.getStatus());
            this.sqlConsulta.setString(5, pro.getCodigo());

            this.sqlConsulta.executeUpdate();
            this.desconectar();

        } catch (SQLException e) {
            System.err.println("Surgio un error al actualizar el producto" + e.getMessage());
        }
    }
}





    @Override
    public void habilitar(Object objeto) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) objeto;
        
        String consulta = "update productos set status = 1 where codigo = ?";
        
        if(this.conectar()){
            try{
               System.err.println("Se conecto");
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                this.sqlConsulta.setString(1, pro.getCodigo());
                this.sqlConsulta.executeUpdate();
                this.desconectar();            
            }catch (SQLException e) {
                System.err.println("Surgio un error al habilitar " + e.getMessage());
            }
        }
    }

    @Override
    public void deshabilitar(Object objeto) throws Exception {
        Productos pro = (Productos) objeto;

        String consulta = "update productos set status = 0 where codigo = ?";

        if (this.conectar()) {
            try {
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                this.sqlConsulta.setString(1, pro.getCodigo());
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            } catch (SQLException e) {
                System.err.println("Surgio un error al deshabilitar " + e.getMessage());
            }
        }
    }

    @Override
    public boolean isExiste(String codigo) throws Exception {
        String consulta = "SELECT COUNT(*) FROM productos WHERE codigo = ?";
        boolean existe = false;

        if (this.conectar()) {
            try {
                System.out.println("Se conectó.. buscando productos con el código: " + codigo);
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                this.sqlConsulta.setString(1, codigo);
                ResultSet rs = this.sqlConsulta.executeQuery();
                if (rs.next()) {
                    int count = rs.getInt(1);
                    existe = (count > 0);
                }
            } catch (SQLException e) {
                System.err.println("Error al verificar la existencia del producto: " + e.getMessage());
                throw e;
            } finally {
                this.desconectar();
            }
        }
        return existe;
    }

    @Override
    public ArrayList listar() throws Exception {
        ArrayList<Productos> lista = new ArrayList<Productos>();
        Productos pro;
        if(this.conectar()){
            String consulta = "Select * from productos where status = 1 order by idProducto";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.registros = this.sqlConsulta.executeQuery();
            //sacar los registros en el arreglo
            while(this.registros.next()){
                pro = new Productos();
                pro.setIdProducto(this.registros.getInt("idProducto"));
                pro.setCodigo(this.registros.getString("codigo"));
                pro.setPrecio(this.registros.getFloat("precio"));
                pro.setFecha(this.registros.getString("fecha"));
                pro.setStatus(this.registros.getInt("status"));
                pro.setNombre(this.registros.getString("nombre"));
                lista.add(pro);
            }
        }
        this.desconectar();
        return lista;
    }
    
    public ArrayList listarDesactivados() throws Exception {
        ArrayList<Productos> lista = new ArrayList<Productos>();
        Productos pro;
        if(this.conectar()){
            String consulta = "Select * from productos where status = 0 order by idProducto";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.registros = this.sqlConsulta.executeQuery();
            //sacar los registros en el arreglo
            while(this.registros.next()){
                pro = new Productos();
                pro.setIdProducto(this.registros.getInt("idProducto"));
                pro.setCodigo(this.registros.getString("codigo"));
                pro.setPrecio(this.registros.getFloat("precio"));
                pro.setFecha(this.registros.getString("fecha"));
                pro.setStatus(this.registros.getInt("status"));
                pro.setNombre(this.registros.getString("nombre"));
                lista.add(pro);
            }
        }
        this.desconectar();
        return lista;
    }

    @Override
    public ArrayList<Productos> listar(String criterio) throws Exception {
        ArrayList<Productos> lista = new ArrayList<Productos>();
        Productos pro;
        if (this.conectar()) {
            String consulta = "SELECT * FROM productos WHERE codigo = ? AND status = 1 ORDER BY idProducto";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, criterio);
            this.registros = this.sqlConsulta.executeQuery();
            //sacar los registros en el arreglo
            while (this.registros.next()) {
                pro = new Productos();
                pro.setIdProducto(this.registros.getInt("idProducto"));
                pro.setCodigo(this.registros.getString("codigo"));
                pro.setPrecio(this.registros.getFloat("precio"));
                pro.setFecha(this.registros.getString("fecha"));
                pro.setStatus(this.registros.getInt("status"));
                pro.setNombre(this.registros.getString("nombre"));
                lista.add(pro);
            }
        }
        this.desconectar();
        return lista;
    }

    @Override
    public Productos buscar(int id) throws Exception {
       Productos pro = new Productos();
        if(this.conectar()){
            String consulta = "Select * from productos where codigo =?";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            //asignat valores
            this.sqlConsulta.setString(1, String.valueOf(id));
            //hacer consulta
            this.registros= this.sqlConsulta.executeQuery();
            if(this.registros.next()){
                pro.setIdProducto(this.registros.getInt("idProducto"));
                pro.setCodigo(this.registros.getString("codigo"));
                pro.setPrecio(this.registros.getFloat("precio"));
                pro.setFecha(this.registros.getString("fecha"));
                pro.setStatus(this.registros.getInt("status"));
                pro.setNombre(this.registros.getString("nombre"));
            }
        }
        this.desconectar();
        return pro;
    }

    @Override
    public Productos buscar(String codigo) throws Exception {
        Productos pro = new Productos();
        if(this.conectar()){
            String consulta = "Select * from productos where codigo =?";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            //asignat valores
            this.sqlConsulta.setString(1, codigo);
            //hacer consulta
            this.registros= this.sqlConsulta.executeQuery();
            if(this.registros.next()){
                pro.setIdProducto(this.registros.getInt("idProducto"));
                pro.setCodigo(this.registros.getString("codigo"));
                pro.setPrecio(this.registros.getFloat("precio"));
                pro.setFecha(this.registros.getString("fecha"));
                pro.setStatus(this.registros.getInt("status"));
                pro.setNombre(this.registros.getString("nombre"));
            }
        }
        this.desconectar();
        return pro;
    }
}
