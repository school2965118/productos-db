package models;


public class TestDb {

    public static void main(String[] args) {
        
        Productos pro = new Productos();
        DBProducto db = new DBProducto();
        
        pro.setCodigo("0011010");
        pro.setNombre("Avena");
        pro.setPrecio(34.60f);
        pro.setFecha("2023-6-21");
        pro.setStatus(0);
        
        try {
            db.insertar(pro);
            System.out.println("Producto creado con exito.");

            Productos res = new Productos();
            res = (Productos) db.buscar("0011010");
            System.out.println("Nombre:" + res.getNombre() + "Precio: " + res.getPrecio());
        } catch (Exception e) {
            System.out.println("Surgio un error rarisimo " + e.getMessage());
        }
    }

}
