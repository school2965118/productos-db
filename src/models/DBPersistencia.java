/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package models;
import java.util.ArrayList;

public interface DBPersistencia {
    public void insertar(Object objeto) throws Exception;
    public void actualizar(Object objeto) throws Exception;
    public void habilitar(Object objeto) throws Exception;
    public void deshabilitar(Object objeto) throws Exception;
    public boolean isExiste(String codigo) throws Exception;
    public ArrayList listar() throws Exception;
    public ArrayList listar(String criterio) throws Exception;
    public Object buscar(int id) throws Exception;
    public Object buscar(String codigo) throws Exception;
}